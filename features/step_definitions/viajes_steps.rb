Given(/^Juan tiene un saldo de \$(\d+) en la SUBE$/) do |saldo|
  @sube = Sube.new saldo
end

And(/^el viaje cuesta \$(\d+)$/) do |precio|
  @viaje = Viaje.new(precio)
end

When(/^Juan intenta pagar el viaje$/) do
  begin
    @sube.pagar @viaje
  rescue InsufficientBalanceError
    @failed_trip = true
  end
end

Then(/^el viaje es rechazado$/) do
  expect(@failed_trip).to be_true
end

And(/^el saldo de Juan es \$(\d+)$/) do |saldo|
  expect(@sube.saldo).to eq(saldo)
end