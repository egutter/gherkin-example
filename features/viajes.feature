Feature: Saldo insuficiente

  Scenario: El viaje excede el saldo disponible
    Given Juan tiene un saldo de $10 en la SUBE
    And el viaje cuesta $16
    When Juan intenta pagar el viaje
    Then el viaje es rechazado
    And el saldo de Juan es $10

