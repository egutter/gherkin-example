class Sube

  attr_reader :saldo

  def initialize saldo
    @saldo = saldo
  end

  def pagar viaje
    raise InsufficientBalanceError.new('Saldo insuficiente') if viaje.precio > @saldo
  end
end